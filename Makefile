ifndef IMAGE_NAME
    IMAGE_NAME=selenium-robot
endif
ifndef IMAGE_REGISTRY
    IMAGE_REGISTRY=10.129.193.152:5000
endif
ifndef UBUNTU_IMAGE_NAME
    UBUNTU_IMAGE_NAME=ubuntu
endif
ifndef IMAGE_VERSION
    IMAGE_VERSION=1.0.3
endif

DOCKERFILES =  latest.dockerfile 
.DELETE_ON_ERROR:

%.dockerfile: dockerfile.template
	echo "$@ $< $* \n"
	cp $< $@
	sed -i 's/PARAM_IMAGE_REGISTRY/$(IMAGE_REGISTRY)/g' $@
	sed -i 's/PARAM_UBUNTU_IMAGE_NAME/$(UBUNTU_IMAGE_NAME)/g' $@
	sed -i 's/PARAM_UBUNTU_IMAGE_VERSION/$*/g' $@
	docker build -f $@ -t $(IMAGE_REGISTRY)/$(IMAGE_NAME):$(IMAGE_VERSION) .

deploy: build
	docker push  $(IMAGE_REGISTRY)/$(IMAGE_NAME):$(IMAGE_VERSION)

build: $(DOCKERFILES)

clean:
	rm -rf $(DOCKERFILES)
