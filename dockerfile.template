FROM PARAM_IMAGE_REGISTRY/PARAM_UBUNTU_IMAGE_NAME:PARAM_UBUNTU_IMAGE_VERSION

# https://hub.docker.com/r/qualifylabs/robotframework-appium-docker/~/dockerfile/

MAINTAINER "William Melchior Jablonski" <williamjablonski@dellemc.com>

ENV DEBIAN_FRONTEND nointeractive
ENV GECKODRIVER_VER v0.15.0
ENV GECKODRIVER_URL https://github.com/mozilla/geckodriver/releases/download/${GECKODRIVER_VER}/geckodriver-${GECKODRIVER_VER}-linux64.tar.gz

RUN apt-get update -y \
    && apt-get install -y curl python-pip firefox dbus-x11 ttf-wqy-microhei \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/* \
    && cd /usr/local/bin \
    && curl -L ${GECKODRIVER_URL} | tar xz \
    && pip install robotframework robotframework-selenium2library selenium

RUN apt-get -qq update -y \
    && apt-get -qq install -y libxss1 libappindicator1 libindicator7 xvfb unzip wget

RUN wget -k https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 

RUN apt-get -qq update -y \
    && apt-get -qq install -y libnspr4 libnss3 xdg-utils

RUN apt-get -qq update -y \
    && apt-get -qq install -y fonts-liberation

RUN  dpkg -i google-chrome*.deb 

RUN wget -N http://chromedriver.storage.googleapis.com/2.26/chromedriver_linux64.zip \
    && unzip chromedriver_linux64.zip \
    && chmod +x chromedriver \
    && mv -f chromedriver /usr/local/share/chromedriver \
    && ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver \
    && ln -s /usr/local/share/chromedriver /usr/bin/chromedriver

WORKDIR /robots

ADD . /robots

RUN chmod +x config/install.sh \
&& chmod +x run/run.sh \
&& /robots/config/install.sh


CMD ["/robots/run/run.sh"]
