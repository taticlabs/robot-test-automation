#!/usr/bin/env bash
# Selenium
pip install --upgrade --pre robotframework-selenium2library
# Robot
pip install robotframework
# SauceClient
pip install sauceclient
# HTTP
pip install --upgrade robotframework-httplibrary