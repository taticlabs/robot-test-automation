*** Settings ***
Documentation   MEU VIVO
Force Tags      smoke-test-meu-vivo
Metadata    Responsavel        williamjablonski@dellemc.com
Library     SeleniumLibrary
Library     OperatingSystem
Suite Setup  Setup chromedriver
*** Variables ***
${URL}=
${USERNAME}=
${PASSWORD}=
...
*** Test Cases ***
SMOKE TEST Login meu vivo - menu superior
    Given o site esta aberto
    When eu seleciono o estado SP
    When eu clico em sou cliente
    When eu vou para a tela de login do portal
    When eu preencho o usuario
    When eu preencho a senha
    When eu clico em entrar
    Then o login é executado com sucesso
*** Keywords ***
Setup chromedriver
|  |  Set Environment Variable  | webdriver.chrome.driver  |  C:\chromedriver_win32\chromedriver.exe
o site esta aberto
|  |  Open Browser	|  ${URL}  |  Chrome
|  |  Maximize Browser Window
eu seleciono o estado SP
|  |  Wait Until Element Is Enabled  |  id=campoRegional  |  timeout=60
|  |  Input Text    |  id=campoRegional  |  São Paulo
|  |  Click Element   |   xpath=//*[@class='ui-menu-item-custom']
eu clico em sou cliente
|  |  Click Element    |  xpath=//html/body/div[6]/div/div[2]/div[1]/div[6]/a[1]
eu vou para a tela de login do portal
|  |  Click Element    |  xpath=//*[@id="header"]/div/div/div[2]/div[1]/a/img
eu preencho o usuario
|  |  Wait Until Page Contains Element |  xpath=//*[@id='campo_login']
|  |  Input Text      |  xpath=//*[@id="cpfOuEmail"]  |  ${USERNAME}
eu preencho a senha
|  |  Sleep	 |  3
|  |  Wait Until Element Is Enabled  |  xpath=//*[@id="senhaText"]  |  timeout=60
|  |  Click Element   |  xpath=//*[@id="senhaText"]
|  |  Log    |   ${PASSWORD}
|  |  Sleep	 |
|  |  Set Focus To Element	 |  xpath=//*[@id="senhaText"]
|  |  Input Text     |  xpath=//*[@id="senhaText"] |   ${PASSWORD}
eu clico em entrar
|  |  Wait Until Element Is Enabled  |  id=btn-entrar-login-we  |  timeout=60
|  |  Click Element    |  xpath=//*[@id="btnEntrar"]
o login é executado com sucesso
|  |  Wait Until Element Is Enabled  |  xpath=//*[@id='m1']/div/a
