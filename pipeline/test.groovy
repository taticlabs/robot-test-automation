def envMap = [
  esteira01: './run/run-meuvivo-ESTEIRA1.sh',
  esteira02: './run/run-meuvivo-ESTEIRA2.sh',
  esteira03: './run/run-meuvivo-ESTEIRA3.sh',
  preprod:   './run/run-meuvivo-PRE-PROD.sh',
  prod:      './run/run-meuvivo-PROD.sh'
]

steps.container("selenium-robot"){
  def script = envMap[ENVIRONMENT]
  steps.echo "Executar Smoke Tests"
  steps.sh "chmod +x ${script}"
  steps.sh "${script}"
}
